import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

const ROUND_ROCK = 'O';
const EMPTY_SPACE = '.';
const CYCLES = 1000000000;

function parseFile(lines: string[]): string[][] {
  return lines.slice(0, -1).map(line => line.split(''));
}

function solve(platform: string[][]): number {
  let load: number = 0;
  
  let states: { [key: string]: number } = {};

  let current: number = 1;
  let loopIndex: number | undefined = undefined;
  while (current <= CYCLES) {
    platform = cycle(platform);
    const stateKey = platform.map(line => line.join('')).join('');
    loopIndex = states[stateKey];

    if (!loopIndex) {
      states[stateKey] = current;
    } else {
      const loopLength = current - loopIndex;
      for (let i = 0; i < (CYCLES - current) % loopLength; i++) {
        platform = cycle(platform);
      }
      load = calculateLoad(platform);
      break;
    }
    current++;
  }
  
  return load;
}

function cycle(platform: string[][]): string[][] {
  const max = platform.length; // platform is square
  for (let i = 0; i < 4; i++) { // Rotate 4 times
    // Displace rocks
    for (let x = 1; x < max; x++) {
      for (let y = 0; y < max; y++) {
        const char = platform[x][y];
        if (char === ROUND_ROCK) {
          const newX = moveRock(platform, x, y);
          if (newX !== x) {
            platform[newX][y] = ROUND_ROCK;
            platform[x][y] = EMPTY_SPACE;
          }
        }
      }
    }
    platform = rotateMatrix(platform);
  }
  return platform;
}

function moveRock(platform: string[][], startRow: number, col: number): number {
  for (let i = startRow - 1; i >= 0; i--) {
    const char = platform[i][col];
    if (char !== EMPTY_SPACE) {
      return i + 1;
    }
  }
  return 0;
}

// Thanks ChatGPT
function rotateMatrix(matrix: string[][]): string[][] {
  const size = matrix.length;

  // Transpose the matrix
  const transposedMatrix: string[][] = [];
  for (let row = 0; row < size; row++) {
    transposedMatrix.push([]);
    for (let col = 0; col < size; col++) {
      transposedMatrix[row][col] = matrix[col][row];
    }
  }

  // Reverse each row of the transposed matrix
  return transposedMatrix.map(row => row.reverse());
}

function calculateLoad(platform: string[][]): number {
  let sum: number = 0;
  const max = platform.length;
  for (const [i, row] of Object.entries(platform)) {
    const rocks: number = row.filter(c => c === ROUND_ROCK).length;
    sum += rocks * (max - parseInt(i, 10));
  }
  return sum;
}
