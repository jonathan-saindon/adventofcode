import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

const ROUND_ROCK = 'O';
const EMPTY_SPACE = '.';

function parseFile(lines: string[]): string[][] {
  return lines.slice(0, -1).map(line => line.split(''));
}

function solve(platform: string[][]): number {
  let sum = 0;

  // Displace rocks
  const maxX = platform.length;
  const maxY = platform[0].length;
  for (let x = 1; x < maxX; x++) {
    for (let y = 0; y < maxY; y++) {
      const char = platform[x][y];
      if (char === ROUND_ROCK) {
        const newX = moveRock(platform, x, y);
        if (newX !== x) {
          platform[newX][y] = ROUND_ROCK;
          platform[x][y] = EMPTY_SPACE;
        }
      }
    }
  }

  // Calculate load
  const max = platform.length;
  for (const [i, row] of Object.entries(platform)) {
    const rocks: number = row.filter(c => c === ROUND_ROCK).length;
    sum += rocks * (max - parseInt(i, 10));
  }
  
  return sum;
}

function moveRock(platform: string[][], startRow: number, col: number): number {
  for (let i = startRow - 1; i >= 0; i--) {
    const char = platform[i][col];
    if (char !== EMPTY_SPACE) {
      return i + 1;
    }
  }
  return 0;
}
