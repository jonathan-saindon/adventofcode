import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

const MAX_RED = 12;
const MAX_GREEN = 13;
const MAX_BLUE = 14;

readLinesFromFile(filePath).then(solve).then((value) => console.log(value));

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

function solve(lines: string[]): number {
  let total: number = 0;
  for (const line of lines) {
    if (line.length === 0) { continue; }
    let isGamePossible = true;
    const [gameName, allSets] = line.split(': ');
    const [_, gameId] = gameName.split(' ');
    const sets = allSets.split('; ');
    for (const set of sets) {
      if (!isGamePossible) { break; }
      const hands = set.split(', ');
      for (const hand of hands) {
        const [nBallsStr, color] = hand.split(' ');
        const nBalls = parseInt(nBallsStr);
        if (color === 'red'   && nBalls > MAX_RED
         || color === 'green' && nBalls > MAX_GREEN
         || color === 'blue'  && nBalls > MAX_BLUE
        ) {
          isGamePossible = false;
          break;
        }
      }
    }
    if (isGamePossible) {
      total += parseInt(gameId);
    }
  }
  return total;
}
