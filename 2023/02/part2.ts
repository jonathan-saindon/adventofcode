import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(solve).then((value) => console.log(value));

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

function solve(lines: string[]): number {
  let total: number = 0;
  for (const line of lines) {
    let minRed: number = 0, minGreen: number = 0, minBlue: number = 0;
    if (line.length === 0) { continue; }

    const [_, allSets] = line.split(': ');
    const sets = allSets.split('; ');
    for (const set of sets) {
      const hands = set.split(', ');
      for (const hand of hands) {
        const [nBallsStr, color] = hand.split(' ');
        const nBalls = parseInt(nBallsStr);
        if (color === 'red') {
          minRed = nBalls > minRed ? nBalls : minRed;
        } else if (color === 'green') {
          minGreen = nBalls > minGreen ? nBalls : minGreen;
        } else if (color === 'blue') {
          minBlue = nBalls > minBlue ? nBalls : minBlue;
        }
      }
    }
    total += (minRed * minGreen * minBlue);
  }
  return total;
}
