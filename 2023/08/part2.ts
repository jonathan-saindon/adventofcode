import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

type Directions = { L: string, R: string }
type NodeMap = { [key: string]: Directions };

function parseFile(lines: string[]): [string[], NodeMap] {
  const instructions: string[] = lines[0].split('');
  let map: NodeMap = {};
  for (const node of lines.slice(2, lines.length - 1)) {
    const [name, directions] = node.split(' = ');
    const [left, right] = directions.slice(1, directions.length - 1).split(', ');
    map[name] = { L: left, R: right };
  }
  return [instructions, map];
}

function solve([instructions, map]: [string[], NodeMap]): number {
  const maxIndex: number = instructions.length - 1;

  const nodes: string[] = Object.keys(map).filter(node => node[2] == 'A');
  const steps: number[] = nodes.map(node => {
    let steps: number = 0;
    let index: number = 0;
    do {
      node = map[node][instructions[index] as keyof Directions];
      index = index == maxIndex ? 0 : index + 1;
      steps++;
    } while (node[2] != 'Z');
    return steps;
  });

  // Find LCM of all steps values
  return steps.reduce((result, n) => lcm(result, n));
}

function lcm(a: number, b: number): number {
  return (a * b) / gcd(a, b);
}

// Source: https://stackoverflow.com/questions/17445231/js-how-to-find-the-greatest-common-divisor
function gcd(a: number, b: number): number {
  return b === 0 ? a : gcd(b, a % b);
}
