import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

type Directions = { L: string, R: string }
type NodeMap = { [key: string]: Directions };

function parseFile(lines: string[]): [string[], NodeMap] {
  const instructions: string[] = lines[0].split('');
  let map: NodeMap = {};
  for (const node of lines.slice(2, lines.length - 1)) {
    const [name, directions] = node.split(' = ');
    const [left, right] = directions.slice(1, directions.length - 1).split(', ');
    map[name] = { L: left, R: right };
  }
  return [instructions, map];
}

function solve([instructions, map]: [string[], NodeMap]): number {
  const maxIndex: number = instructions.length - 1;
  const goal: string = 'ZZZ';

  let node: string = 'AAA';
  let steps: number = 0;
  let index: number = 0;
  do {
    node = map[node][instructions[index] as keyof Directions];
    index = index == maxIndex ? 0 : index + 1;
    steps++;
  } while (node != goal);

  return steps;
}
