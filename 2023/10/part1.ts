import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

type Position = {
  x: number,
  y: number,
}

function parseFile(lines: string[]): string[][] {
  return lines.slice(0, lines.length - 1).map(line => line.split(''));
}

function solve(map: string[][]): number {
  let origin: Position = findAnimalPosition(map);
  let firstMove: Position = findFirstMove(origin, map);

  let current: Position = firstMove;
  let previous: Position = origin;

  let pipeLength: number = 1; // We've already taken 1 step
  do {
    const tmp = getNextPosition(current, previous, map[current.x][current.y]);
    previous = current;
    current = tmp;
    pipeLength++;
  } while (current.x != origin.x || current.y != origin.y);

  return pipeLength / 2; // Furthest point is at the middle of the pipe
}

function findFirstMove(origin: Position, map: string[][]): Position {
  if (['|', '7', 'F'].includes(map[origin.x - 1][origin.y])) { return { x: origin.x - 1, y: origin.y } };
  if (['|', 'J', 'L'].includes(map[origin.x + 1][origin.y])) { return { x: origin.x + 1, y: origin.y } };
  if (['-', 'J', '7'].includes(map[origin.y + 1][origin.y])) { return { x: origin.x, y: origin.y + 1 } };
  if (['-', 'J', '7'].includes(map[origin.y - 1][origin.y])) { return { x: origin.x, y: origin.y - 1 } };
  return origin; // No pipe around animal. Should not happen.
}

function findAnimalPosition(map: string[][]): Position {
  for (let x = 0; x < map.length; x++) {
    for (let y = 0; y < map[x].length; y++) {
      if (map[x][y] == 'S') { return { x, y }; }
    }
  }
  return { x: -1, y: -1 }; // No animal found. Should not happen.
}

function getNextPosition(current: Position, previous: Position, tile: string): Position {
  switch(tile) {
    case 'F':
      return previous.x > current.x ? { x: current.x, y: current.y + 1 } : { x: current.x + 1, y: current.y };
    case '7':
      return previous.x > current.x ? { x: current.x, y: current.y - 1 } : { x: current.x + 1, y: current.y };
    case 'J':
      return previous.x < current.x ? { x: current.x, y: current.y - 1 } : { x: current.x - 1, y: current.y };
    case 'L':
      return previous.x < current.x ? { x: current.x, y: current.y + 1 } : { x: current.x - 1, y: current.y };
    case '-':
      return previous.y > current.y ? { x: current.x, y: current.y - 1 } : { x: current.x, y: current.y + 1 };
    case '|':
      return previous.x > current.x ? { x: current.x - 1, y: current.y } : { x: current.x + 1, y: current.y };
    default:
      return current;
  }
}