import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

type Position = {
  x: number,
  y: number,
}

const ANIMAL = 'S';
  
function parseFile(lines: string[]): string[][] {
  return lines.slice(0, lines.length - 1).map(line => line.split(''));
}

function solve(map: string[][]): number {
  const pipe: Position[] = findPipe(map);
  const maze = createMaze(map, pipe);
  printMaze(maze);
  const mazeSize = maze.length * maze[0].length;
  return mazeSize - countOutsideTiles(maze) - pipe.length;
}

function countOutsideTiles(maze: string[][]): number {
  const maxX = maze.length;
  const maxY = maze[0].length;
  let outside: number = 0;
  for (let x = 0; x < maxX; x++) {
    let isOutside: boolean = true;
    let lastCorner: string = '?';

    for (let y = 0; y < maxY; y++) {
      let tile: string = maze[x][y];
      if (tile != '+') {
        if (tile == '-') { continue; }
        else if (tile == '|') {
          isOutside = !isOutside;
        }
        else if (tile == 'J') {
          if (lastCorner == 'F') {
            isOutside = !isOutside;
          }
          lastCorner = 'J';
        } 
        else if (tile == '7') {
          if (lastCorner == 'L') {
            isOutside = !isOutside;
          }
          lastCorner = '7';
        }
        else if (tile == 'F') {
          lastCorner = 'F';
        }
        else if (tile == 'L') {
          lastCorner = 'L';
        }
      }
      else if (isOutside) {
        if (maze[x][y] != 'O') {
          maze[x][y] = 'O';
          outside++;
        }
      } else {
      }
    }
  }
  return outside;
}

function findPipe(map: string[][]): Position[] {
  const animalPosition: Position = findAnimalPosition(map);
  let current: Position = findFirstMove(animalPosition, map);
  let previous: Position = animalPosition;
  let pipe: Position[] = [current];
  do {
    const tmp = getNextPosition(current, previous, map[current.x][current.y]);
    previous = current;
    current = tmp;
    pipe.push(current);
  } while (current.x != animalPosition.x || current.y != animalPosition.y);
  return pipe;
}

function findAnimalPosition(map: string[][]): Position {
  const maxX = map.length;
  const maxY = map[0].length;
  for (let x = 0; x < maxX; x++) {
    for (let y = 0; y < maxY; y++) {
      if (map[x][y] == ANIMAL) {
        return { x, y };
      }
    }
  }
  return { x: -1, y: -1 }; // No animal found. Should not happen.
}

function findFirstMove(origin: Position, map: string[][]): Position {
  if (['|', '7', 'F'].includes(map[origin.x - 1][origin.y])) { return { x: origin.x - 1, y: origin.y } };
  if (['|', 'J', 'L'].includes(map[origin.x + 1][origin.y])) { return { x: origin.x + 1, y: origin.y } };
  if (['-', 'J', '7'].includes(map[origin.y + 1][origin.y])) { return { x: origin.x, y: origin.y + 1 } };
  if (['-', 'J', '7'].includes(map[origin.y - 1][origin.y])) { return { x: origin.x, y: origin.y - 1 } };
  return origin; // No pipe around animal. Should not happen.
}

function getNextPosition(current: Position, previous: Position, tile: string): Position {
  switch(tile) {
    case 'F':
      return previous.x > current.x ? { x: current.x, y: current.y + 1 } : { x: current.x + 1, y: current.y };
    case '7':
      return previous.x > current.x ? { x: current.x, y: current.y - 1 } : { x: current.x + 1, y: current.y };
    case 'J':
      return previous.x < current.x ? { x: current.x, y: current.y - 1 } : { x: current.x - 1, y: current.y };
    case 'L':
      return previous.x < current.x ? { x: current.x, y: current.y + 1 } : { x: current.x - 1, y: current.y };
    case '-':
      return previous.y > current.y ? { x: current.x, y: current.y - 1 } : { x: current.x, y: current.y + 1 };
    case '|':
      return previous.x > current.x ? { x: current.x - 1, y: current.y } : { x: current.x + 1, y: current.y };
    default:
      return current;
  }
}
  
function createMaze(map: string[][], pipe: Position[]): string[][] {
  let maze: string[][] = [];

  // Init maze
  const maxX = map.length;
  const maxY = map[0].length;
  for (let x = 0; x < maxX; x++) {
    if (!maze[x]) {
      maze[x] = [];
    }
    for (let y = 0; y < maxY; y++) {
      maze[x][y] = '+';
    }
  }

  // Add pieces of the main pipe
  for (const piece of pipe) {
    maze[piece.x][piece.y] = map[piece.x][piece.y];
  }

  return maze;
}

function printMaze(maze: string[][]) {
  const niceChars: { [key: string]: string } = {
    '|': '│',
    '-': '─',
    'L': '└',
    'J': '┘',
    '7': '┐',
    'F': '┌',
    '+': '+',
  };
  const maxX = maze.length;
  const maxY = maze[0].length;
  for (let x = 0; x < maxX; x++) {
    let line: string = "";
    for (let y = 0; y < maxY; y++) {
      const charToPrint = niceChars[maze[x][y]] || '+';
      line += charToPrint;
    }
    console.log(line);
  }
}
