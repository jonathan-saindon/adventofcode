import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

function parseFile(lines: string[]) {
  let patterns: string[][] = [];
  let i = 0;
  for (const line of lines) {
    if (!line.length) {
      i++;
      continue;
    }
    if (!patterns[i]) {
      patterns[i] = [];
    }
    patterns[i].push(line);
  }
  return patterns;
}

function solve(patterns: string[][]): number {
  let sum = 0;
  for (const lines of patterns) {
    let h = findReflectionIndex(lines), v = 0;
    if (!h) {
      const rotatesLines = rotateMap90deg(lines.map(line => line.split(''))).map(matrixLine => matrixLine.join(''));
      v = findReflectionIndex(rotatesLines);
    }
    sum += h * 100 + v;
  }
  return sum;
}

function findReflectionIndex(lines: string[]): number {
  for (let i = 1; i <= lines.length - 1; i++) {
    const span = Math.min(lines.length - i, i);
    if (hasReflection(lines, i, span)) {
      return i;
    }
  }
  return 0;
}

function hasReflection(lines: string[], start: number, span: number): boolean {
  for (let s = 1; s <= span; s++) {
    if (lines[start - s] != lines[start + s - 1]) {
      return false;
    }
  }
  return true;
}

function rotateMap90deg(matrix: string[][]): string[][] {
  const rows = matrix.length;
  const cols = matrix[0].length;

  // Transpose the matrix
  const transposedMap: string[][] = [];
  for (let row = 0; row < cols; row++) {
    transposedMap.push([]);
    for (let col = 0; col < rows; col++) {
      transposedMap[row][col] = matrix[col][row];
    }
  }

  return transposedMap;
}
