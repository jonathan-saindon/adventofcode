import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

type Race = {
  time: number,
  record: number,
};

function parseFile(lines: string[]): Race {
  return {
    time: parseInt(lines[0].slice(5).trim().split(/[ ,]+/).join('')),
    record: parseInt(lines[1].slice(9).trim().split(/[ ,]+/).join('')),
  };
}

function solve(race: Race): number {
  let count = 0;
  for (let timeButton = 1; timeButton < race.time - 1; timeButton++) {
    const timeRun = race.time - timeButton;
    const distance = timeButton * timeRun;
    if (distance > race.record) {
      count++;
    }
  }
  return count;
}
