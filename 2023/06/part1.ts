import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

type Race = {
  time: number,
  record: number,
};

function parseFile(lines: string[]): Race[] {
  const times: number[] = lines[0].slice(5).trim().split(/[ ,]+/).map(n => parseInt(n, 10));
  const records: number[] = lines[1].slice(9).trim().split(/[ ,]+/).map(n => parseInt(n, 10));

  let races: Race[] = [];
  for (let i = 0; i < times.length; i++) {
    races.push({
      time: times[i],
      record: records[i],
    });
  }
  return races;
}

function solve(races: Race[]): number {
  let total = 1; // Total will be multiplied with count
  for (const race of races) {
    let count = 0;
    for (let timeButton = 1; timeButton < race.time - 1; timeButton++) {
      const timeRun = race.time - timeButton;
      const distance = timeButton * timeRun;
      if (distance > race.record) {
        count++;
      }
    }
    if (count > 0) {
      total *= count;
    }
  }
  return total;
}
