import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

function solve(lines: string[]): number {
  let sum: number = 0;

  for (const line of lines) {
    let matches: number = 0;
    if (line.length == 0) { continue; }
    const card = buildCardFromLine(line);
    for (const number of card.numbers) {
      if (card.winning.includes(number)) {
        matches++;
      }
    }
    if (matches > 0) {
      sum += 2 ** (matches - 1);
    }
  }

  return sum;
}

type Card = {
  winning: number[],
  numbers: number[],
};

function buildCardFromLine(line: string): Card {
  const [_, data] = line.split(': ');
  const [winningData, numbersData] = data.split(' | ');
  return {
    winning: winningData.split(' ').filter(Boolean).map(n => parseInt(n, 10)),
    numbers: numbersData.split(' ').filter(Boolean).map(n => parseInt(n, 10)),
  };
}