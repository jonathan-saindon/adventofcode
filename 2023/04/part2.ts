import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

function solve(lines: string[]): number {
  const cards: Card[] = lines.map(buildCardFromLine);
  
  for (const [index, card] of cards.entries()) {
    let matches: number = 0;
    for (const number of card.numbers) {
      if (card.winning.includes(number)) {
        cards[index + ++matches].copies += card.copies;
      }
    }
  }

  return cards.reduce((sum, card) => (sum += card.copies), 0);
}

type Card = {
  copies: number,
  winning: number[],
  numbers: number[],
};

function buildCardFromLine(line: string): Card {
  if (!line.length) return { copies: 0, winning: [], numbers: [] };
  const [_, data] = line.split(': ');
  const [winningData, numbersData] = data.split(' | ');
  return {
    copies: 1,
    winning: winningData.split(' ').filter(Boolean).map(n => parseInt(n, 10)),
    numbers: numbersData.split(' ').filter(Boolean).map(n => parseInt(n, 10)),
  };
}
