import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

type SeedRange = {
  number: number,
  length: number,
};

type MapEntry = {
  destination: number,
  source: number,
  length: number,
};

let seeds: SeedRange[] = [];
let maps: MapEntry[][] = [[], [], [], [], [], [], []];

function parseFile(lines: string[]) {
  let step = 1;
  for (const line of lines) {
    if (line.slice(-1) === ':') { continue; }
    if (!line.length) {
      step++;
      continue;
    }
    if (step === 1) {
      const [_, data] = line.split(': ');
      const numbers: string[] = data.split(' ');
      for (let i = 0; i < numbers.length; i+=2) {
        seeds.push({ number: parseInt(numbers[i]), length: parseInt(numbers[i + 1]) });
      }
    } else if (step >= 2) {
      const [destination, source, length] = line.split(' ').map(n => parseInt(n, 10));
      maps[step - 2].push({ destination, source, length });
    }
  }
  for (const map of maps) {
    map.sort((a, b) => a.source - b.source);
  }
}

function solve(): number {
  let lowest: number = -1;
  for (const seed of seeds) {
    for (let seedNumber = seed.number; seedNumber < (seed.number + seed.length); seedNumber++) {
      let destination = seedNumber;
      for (const map of maps) {
        destination = findDestination(destination, map);
      }
      lowest = lowest < 0 ? destination : Math.min(lowest as number, destination);
    }
  }

  return lowest as number;
}

function findDestination(source: number, entries: MapEntry[]): number {
  for (const entry of entries) {
    if (source >= entry.source && source <= (entry.source + entry.length - 1)) {
      return entry.destination + (source - entry.source);
    }
  }
  return source;
}