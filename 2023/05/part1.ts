import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

type MapEntry = {
  destination: number,
  source: number,
  length: number,
};

let seeds: number[] = [];
let maps: MapEntry[][] = [[], [], [], [], [], [], []];

function parseFile(lines: string[]) {
  let step = 1;
  for (const line of lines) {
    if (line.slice(-1) === ':') { continue; }
    if (!line.length) {
      step++;
      continue;
    }
    if (step === 1) {
      const [_, data] = line.split(': ');
      for (const seedNumber of data.split(' ')) {
        seeds.push(parseInt(seedNumber));
      }
    } else if (step >= 2) {
      const [destination, source, length] = line.split(' ').map(n => parseInt(n, 10));
      maps[step - 2].push({ destination, source, length });
    }
  }
  for (const map of maps) {
    map.sort((a, b) => a.source - b.source);
  }
}

function solve(): number {
  let lowest: number = -1;
  for (const seed of seeds) {
    let destination: number = seed;
    for (const map of maps) {
      for (const entry of map) {
        if (destination >= entry.source && destination <= (entry.source + entry.length - 1)) {
          destination = entry.destination + (destination - entry.source);
          break;
        }
      }
    }
    lowest = lowest < 0 ? destination : Math.min(lowest as number, destination);
  }
  return lowest as number;
}
