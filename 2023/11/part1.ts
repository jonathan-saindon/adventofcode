import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

const GALAXY = '#';
const GALAXY_PATTERN = /#/g;

type Coordinate = [number, number];

function parseFile(lines: string[]): Coordinate[] {
  const map: string[][] = lines.slice(0, lines.length - 1).map(line => line.split(''));

  const vExpansions = findExpansions(map);
  const hExpansions = findExpansions(rotateMap90deg(map));

  const galaxies: Coordinate[] = [];
  for (const [index, line] of Object.entries(map)) {
    if (line.indexOf(GALAXY) < 0) { continue; }
    const iter = line.join('').matchAll(GALAXY_PATTERN);
    let match;
    while ((match = iter.next()).value) {
      const baseX: number = parseInt(index, 10);
      const baseY: number = parseInt(match.value.index, 10);
      const offsetX = vExpansions.filter(x => x < baseX).length;
      const offsetY = hExpansions.filter(y => y < baseY).length;
      galaxies.push([baseX + offsetX, baseY + offsetY]);
    }
  }
  return galaxies;
}

function solve(galaxies: Coordinate[]): number {
  let sum: number = 0;
  for (const [indexA, a] of Object.entries(galaxies)) {
    for (const [indexB, b] of Object.entries(galaxies).slice(parseInt(indexA, 10), galaxies.length)) {
      if (indexA == indexB) { continue; } // Don't measure distance with self
      sum += measureDistance(a, b);
    }
  }
  return sum;
}

function measureDistance(a: Coordinate, b: Coordinate): number {
  const distanceX = Math.abs(a[0] - b[0]);
  const distanceY = Math.abs(a[1] - b[1]);
  return distanceX + distanceY;
}

function findExpansions(map: string[][]): number[] {
  let expansions: number[] = []
  for (let [index, row] of Object.entries(map)) {
    if (row.indexOf(GALAXY) < 0) {
      expansions.push(parseInt(index, 10));
    }
  }
  return expansions;
}

// Thanks chatGPT
function rotateMap90deg(map: string[][]): string[][] {
  const rows = map.length;
  const cols = map[0].length;

  // Transpose the map
  const transposedMap: string[][] = [];
  for (let row = 0; row < cols; row++) {
    transposedMap.push([]);
    for (let col = 0; col < rows; col++) {
      transposedMap[row][col] = map[col][row];
    }
  }

  return transposedMap;
}