import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

function parseLines(lines: string[]) {
  let totalCalibValue: number = 0;
  for (let line of lines) {
    if (line.length === 0) { continue; } // Skip empty line

    // Normalize word digits into number digits
    // This also fixes issues with overlaps like 'twone'
    line = line.replace(/one/g, "one1one")
      .replace(/two/g, "two2two")
      .replace(/three/g, "three3three")
      .replace(/four/g, "four4four")
      .replace(/five/g, "five5five")
      .replace(/six/g, "six6six")
      .replace(/seven/g, "seven7seven")
      .replace(/eight/g, "eight8eight")
      .replace(/nine/g, "nine9nine");

    const rawDigits = line.match(/\d{1}/ig);
    if (!rawDigits) { continue; } // Skip if no digits are found

    // String concat the first and last digit (reuse first digit if it's alone)
    const calibrationValue = rawDigits[0] + (rawDigits.length > 1 ? rawDigits[rawDigits.length - 1] : rawDigits[0]);

    totalCalibValue += parseInt(calibrationValue);
  }
  return totalCalibValue;
}

readLinesFromFile(filePath).then(parseLines).then(console.log);
