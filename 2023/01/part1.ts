import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

const numberRegex = /\d{1}/g;

readLinesFromFile(filePath).then(parseLines).then((value) => console.log(value));

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

function parseLines(lines: string[]) {
  let totalCalibValue = 0;
  for (const line of lines) {
    if (line.length === 0) { continue; } // Skip empty line
    console.log("LINE:", line);

    const digits = line.match(numberRegex);
    if (!digits) { continue; } // Skip if no digits are found

    const calibrationValue = digits.length > 1 ? digits[0] + digits[digits.length - 1] : digits[0] + digits[0];
    console.log("CURRENT CALIB:", calibrationValue);

    totalCalibValue += parseInt(calibrationValue);

    console.log("TOTAL:", totalCalibValue)
  }
  return totalCalibValue;
}
