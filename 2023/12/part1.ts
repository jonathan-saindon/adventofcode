import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

type Data = {
  springs: string,
  groups: number[],
}

function parseFile(lines: string[]): Data[] {
  return lines.slice(0, -1).map(line => {
    const [springs, groups] = line.split(' ');
    return { springs: springs, groups: groups.split(',').map(n => parseInt(n, 10)) };
  });
}

function solve(datas: Data[]): number {
  let sum = 0;
  for (const data of datas) {
    const { springs, groups } = data;
    sum += countValidCombination(springs, groups, 0);
  }
  return sum;
}

/*
 * Brute force all possible combinations
 * Idea taken from https://www.youtube.com/watch?v=xTGkP2GNmbQ
 */
function countValidCombination(springs: string, groups: number[], index: number): number {
  const spring = springs.charAt(index);
  if (index == springs.length) {
    // All wildcards have been replaced. Check if combination is valid or not.
    return isValid(springs, groups) ? 1 : 0;
  } else if (spring === '?') {
    return countValidCombination(springs.slice(0, index) + "#" + springs.slice(index + 1, springs.length), groups, index + 1)
      + countValidCombination(springs.slice(0, index) + "." + springs.slice(index + 1, springs.length), groups, index + 1);
  } else {
    return countValidCombination(springs, groups, index + 1);
  }
}

function isValid(springs: string, groupsToMatch: number[]): boolean {
  const groups: number[] = springs.split('.').map(spring => spring.length).filter(Boolean);
  return groups.toString() == groupsToMatch.toString();
}
