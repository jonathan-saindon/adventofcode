import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

enum HandType {
  HighCard = "HC",
  Pair = "P",
  TwoPairs = "TP",
  ThreeOAK = "T",
  FullHouse = "FH",
  FourOAK = "FO",
  FiveOAK = "FI"
}

type Hand = {
  cards: string,
  bid: number,
  type: HandType,
};

const CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'];

function parseFile(lines: string[]): Hand[] {
  let hands: Hand[] = [];
  for (const line of lines) {
    if (!line.length) { continue; }
    const [cards, bid] = line.split(' ');
    hands.push({
      cards,
      bid: parseInt(bid, 10),
      type: resolveHandType(cards),
    });
  }
  return hands;
}

function resolveHandType(cards: string): HandType {
  // Count cards by value
  const cardMap: { [card: string]: number } = {};
  for (const card of cards) {
    cardMap[card] = (cardMap[card] ?? 0) + 1;
  }

  const cardCounts = Object.values(cardMap).sort((a, b) => b - a);

  // Resolve hand type
  if (cardCounts[0] == 5) { return HandType.FiveOAK; }
  if (cardCounts[0] == 4) { return HandType.FourOAK; }
  if (cardCounts[0] == 3) { return cardCounts[1] == 2 ? HandType.FullHouse : HandType.ThreeOAK; }
  if (cardCounts[0] == 2) { return cardCounts[1] == 2 ? HandType.TwoPairs : HandType.Pair; }
  return HandType.HighCard;
}

function solve(hands: Hand[]): number {
  let total: number = 0;
  let index: number = 0;

  for (const type of Object.values(HandType)) {
    const handsOfType: Hand[] = hands.filter(hand => hand.type == type).sort(compareHands);
    for (const hand of handsOfType) {
      total += hand.bid * (++index);
    }
  }

  return total;
}

function getCardValue(card: string): number {
  return CARDS.indexOf(card);
}

function compareHands(a: Hand, b: Hand): number {
  const [cardsA, cardsB] = [a, b].map(hand => hand.cards.split('').map(getCardValue));
  for (let i = 0; i < 5; i++) {
    if (cardsA[i] == cardsB[i]) { continue; }
    return cardsA[i] - cardsB[i];
  }
  return 0;
} 