import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

const SYMBOL_REGEX = /[^\d\.]/g;

readLinesFromFile(filePath).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

/* 
 * My first try at this using regex wasn't working but I found this example that also used regex that worked
 * https://github.com/mattdarveniza/advent-of-code-23/blob/main/03/index.ts
 * I added some logic to avoid accessing out of bounds characters
 */
function solve(lines: string[]): number {
  let sum: number = 0;

  for (const [i, line] of lines.entries()) {
    const partNumberMatches = [...line.matchAll(/\d+/g)];

    for (const partNumberMatch of partNumberMatches) {
      const [partNumber] = partNumberMatch;

      const numberIndex = partNumberMatch.index ?? 0;
      const startIndex = (numberIndex > 0) ? numberIndex - 1 : 0;
      const endIndex = startIndex + partNumber.length + ((numberIndex > 0) ? 2 : 1);

      // Lines to check for symbols
      const lineIndices = (i > 0) 
        ? [i - 1, i, i + 1] 
        : (i < lines.length - 1)
          ? [i, i + 1]
          : [i - 1, i];
      const isPartNumber = lineIndices.some(index => lines[index]?.substring(startIndex, endIndex).match(SYMBOL_REGEX));
      if (isPartNumber) {
        sum += parseInt(partNumber);
      }
    }
  }

  return sum;
}
