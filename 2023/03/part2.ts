import { readFile } from 'fs';
import { promisify } from 'util';

const filePath = process.argv[2];
const readFileAsync = promisify(readFile);

readLinesFromFile(filePath).then(solve).then(console.log);

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const fileContent = await readFileAsync(filePath, 'utf-8');
  return fileContent.split('\n');
}

function solve(lines: string[]): number {
  let sum: number = 0;

  for (const [i, line] of lines.entries()) {
    const gears = [...line.matchAll(/\*/g)];
    for (const gear of gears) {
      const gearIndex = gear.index ?? 0;
      const gearStartIndex = (gearIndex > 0) ? gearIndex - 1 : 0;
      const gearEndIndex = gearStartIndex + ((gearIndex > 0) ? 2 : 1);
      
      // Lines to check for numbers
      const lineIndices = (i > 0) 
        ? [i - 1, i, i + 1] 
        : (i < lines.length - 1)
          ? [i, i + 1]
          : [i - 1, i];
      
      let ratios: number[] = [];
      lineIndices.forEach(index => {
        const numbers = [...lines[index].matchAll(/\d+/g)];
        for (const number of numbers) {
          const numberValue = number[0];
          const numberIndexStart = number.index ?? 0;
          const numberIndexEnd = numberIndexStart + numberValue.length - 1;
          if ((numberIndexEnd >= gearStartIndex && numberIndexStart <= gearEndIndex)) {
            ratios.push(parseInt(numberValue))
          }
        }
      });

      if (ratios.length == 2) {
        sum += ratios[0] * ratios[1];
      }
      // else if (ratios.length > 2) {
      //   console.log(`STAR ON LINE ${i+1} TOO MANY RATIOS:`, ratios);
      //   break;
      // }
    }
  }

  return sum;
}
