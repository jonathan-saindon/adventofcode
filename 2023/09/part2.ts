import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

function parseFile(lines: string[]): number[][] {
  return lines.slice(0, lines.length - 1).map(line => line.split(' ').map(n => parseInt(n, 10)));
}

function solve(sequences: number[][]): number {
  let sum: number = 0;

  for (const sequence of sequences) {
    let subsequences: number[][] = buildSubsequences(sequence);
    let nextValue: number = getNextValueOfOriginalSequence(subsequences);
    sum += nextValue;
  }

  return sum;
}

function buildSubsequences(sequence: number[]): number[][] {
  let subsequences: number[][] = [sequence];
  do {
    subsequences.push(buildNextSequence(sequence));
    sequence = subsequences[subsequences.length - 1];
  } while (!areAllSameValues(sequence));
  return subsequences;
}

function buildNextSequence(sequence: number[]): number[] {
  let nextSequence: number[] = [];
  for (let i = 1; i < sequence.length; i++) {
    nextSequence.push(sequence[i] - sequence[i - 1]);
  }
  return nextSequence;
}

function areAllSameValues(sequence: number[]): boolean {
  return sequence.every(n => n == sequence[0]);
}

function getNextValueOfOriginalSequence(subsequences: number[][]): number {
  let nextValue = subsequences[subsequences.length - 1][0];
  for (let i = subsequences.length - 2; i >= 0; i--) {
    const tmp = subsequences[i][0] - nextValue;
    subsequences[i] = [tmp, ...subsequences[i]];
    nextValue = tmp;
  }
  return nextValue;
}
