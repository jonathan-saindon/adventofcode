import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

function parseFile(lines: string[]): string[] {
  return lines[0].split(',');
}

type Lens = { label: string, focalLength: number };
// type Box = Lens[];

function solve(steps: string[]): number {
  let boxes: { [key: number]: Lens[] } = {};
  for (const step of steps) {
    const operation: string = step[2];
    const [label, fl] = step.split(operation);
    const focalLength = parseInt(fl, 10)

    let box: Lens[] = getBox(boxes, label);
    let [rank, lens] = getLens(box, label);

    if (operation === '=') {
      // Add lens
      if (lens) {
        // console.log("REPLACE LENS:", lens);
        lens.focalLength = focalLength;
      } else {
        // console.log("ADD NEW LENS");
        box.push({ label, focalLength });
      }
    } else {
      // Remove lens
      if (lens) {
        // console.log("REMOVE LENS:", lens);
        // delete box[rank];
        // box = box.filter(b => b !== undefined);
        box.splice(rank, 1);
      }
    }
  }

  let sum: number = 0;
  for (const [i, box] of Object.entries(boxes)) {
    let index = parseInt(i, 10);
    if (box) {
      // console.log("BOX", i, box);
      for (const [j, lens] of Object.entries(box)) {
        const focalpower = (index + 1) * (parseInt(j, 10) + 1) * (lens as Lens).focalLength;
        console.log("FOCAL POWER OF LENS", `${i}x${j}`, ":", focalpower);
        sum += focalpower;
      }
    }
  }
  return sum;
}

function hash(s: string): number {
  return s.split('').reduce((sum: number, char: string) => ((sum + char.charCodeAt(0)) * 17) % 256, 0);
}

function getBox(boxes: { [key: number]: Lens[] }, label: string) {
  const boxNumber = hash(label);
  if (!boxes[boxNumber]) {
    boxes[boxNumber] = [];
  }
  return boxes[boxNumber];
}

function getLens(box: Lens[], label: string): [number, Lens | undefined] {
  for (const [index, lens] of Object.entries(box)) {
    if (lens.label === label) {
      return [parseInt(index,10), lens];
    }
  }
  return [-1, undefined];
}