import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

function parseFile(lines: string[]): string[] {
  return lines[0].split(',');
}

function solve(steps: string[]): number {
  return steps
    .map(step => step.split('').reduce((sum: number, char: string) => ((sum + char.charCodeAt(0)) * 17) % 256, 0))
    .reduce((sum: number, current: number) => sum + current, 0);
}
