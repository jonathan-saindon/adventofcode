import { readFile } from 'fs';
import { promisify } from 'util';

async function readLinesFromFile(filePath: string): Promise<string[]> {
  const readFileAsync = promisify(readFile);
  return (await readFileAsync(filePath, 'utf-8')).split('\n');
}

const filePath = process.argv[2];
readLinesFromFile(filePath).then(parseFile).then(solve).then(console.log);

// CUSTOM CODE BELOW

function parseFile(lines: string[]) {
  /*
   * Parse each line of the input file
   * Build your dataset to solve the puzzle
   */
  return {};
}

function solve(dataset: unknown): number {
  /*
   * Solve the puzzle using the dataset you've built
   */
  return 0;
}
