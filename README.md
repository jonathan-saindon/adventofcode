# AdventOfCode

This repository contains my solutions to the [Advent of code](https://adventofcode.com/) puzzles

Puzzles descriptions and inputs are downloaded with [aoc-cli](https://github.com/scarvalhojr/aoc-cli)

Each solution file (ie.: `part1.py`) has been validated and outputs the correct answer
