﻿const int TOTAL_DISK = 70000000;
const int TARGET = 30000000;

Node root = BuildFileSystem();
List<Node> directories = new List<Node>();
int totalDiskUsage = DirSize(root);
int unusedSpace = TOTAL_DISK - totalDiskUsage;
int spaceNeeded = TARGET - unusedSpace;
// Console.WriteLine("Space needed " + spaceNeeded);
Console.WriteLine(directories.Where(x => x.size >= spaceNeeded).Min(x => x.size));

Node BuildFileSystem() {
  Node root = new Node("/");
  Node current = root;
  foreach (string line in System.IO.File.ReadLines("input"))
  {
    string[] parts = line.Split(" ");
    if (parts[0] == "$") {
      if (parts[1] != "cd") { continue; } // $ ls
      if (parts[2] == "/") { continue; } // cd /
      string dir = parts[2];
      current = dir == ".." ? current.parent : current.children.First(x => x.name == dir);
    }
    // Directory
    else if (parts[0] == "dir") {
      Node new_node = new Node(parts[1]);
      new_node.parent = current;
      current.children.Add(new_node);
    }
    // File
    else {
      Node new_node = new Node(parts[1], int.Parse(parts[0]));
      new_node.parent = current;
      current.children.Add(new_node);
    }
  }

  return root;
}

int DirSize(Node node) {
  if (node.isFile) {
    return node.size;
  }

  node.size += node.children.Sum(x => DirSize(x));

  directories.Add(node); // Gather all directories in a list

  return node.size;
}

class Node {
  public string name { get; set; }
  public int size { get; set; }
  public Node? parent { get; set; }
  public List<Node> children { get; set; }

  public bool isFile { get { return this.children.Count == 0; } }

  public Node(String name, int size=0) {
    this.name = name;
    this.size = size;
    this.children = new List<Node>();
  }
}