def get_lines():
  inputs = open('input', 'r')
  return inputs.read().splitlines()

def elfs_overlap(elfs):
  elf1, elf2 = elfs.split(',')
  elf1_start, elf1_end = get_work_range(elf1)
  elf2_start, elf2_end = get_work_range(elf2)
  return (
    (elf1_start <= elf2_start and elf1_end >= elf2_start) or
    (elf1_start >= elf2_start and elf1_start <= elf2_end) or
    (elf2_start <= elf1_start and elf2_end >= elf1_start) or
    (elf2_start >= elf1_start and elf2_start <= elf1_end)
  )

def get_work_range(elf):
  s_start, s_end = elf.split('-')
  return int(s_start), int(s_end)

lines = get_lines()
index = 0
overlap_count = 0

for elfs in lines:
  if elfs_overlap(elfs):
    print(f"Elfs duo {index} overlap => {elfs}")
    overlap_count += 1
  index += 1

print(f"Found {overlap_count} complete overlaps")
