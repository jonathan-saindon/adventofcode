def apply_move(piles, move)
  n = move[0].to_i
  from = move[1].to_i - 1
  to = move[2].to_i - 1
  n.times do
    crate = piles[from].pop
    piles[to].push(crate)
  end
  piles
end

piles = nil
File.foreach('input_easy', chomp: true).with_index do |line, index|
  if index.zero?
    piles = line.split(',').map { |pile| pile.scan /\w/ }
    next
  end
  move = line.split(';')
  piles = apply_move(piles, move)
end

puts piles.map { |pile| pile[-1] }.join