def decrypt(move):
    return {
        'A': 'Rock',
        'B': 'Paper',
        'C': 'Scissors',
        'X': 'Rock',
        'Y': 'Paper',
        'Z': 'Scissors'
    }[move]

def get_move_points(move):
    return {
        'X': 1,
        'Y': 2,
        'Z': 3
    }[move]

def get_match_points(moves):
    return {
        'A': {
            'X': 3,
            'Y': 6,
            'Z': 0,
        },
        'B': {
            'X': 0,
            'Y': 3,
            'Z': 6,
        },
        'C': {
            'X': 6,
            'Y': 0,
            'Z': 3,
        }
    }[moves[0]][moves[1]]

inputs = open('input', 'r')
lines = inputs.read().splitlines()

final_score = 0
for line in lines:
    moves = line.split(' ')
    # print(f"Moves: {moves[0]} vs {moves[1]}")

    move_points = get_move_points(moves[1])
    match_points = get_match_points(moves)
    total_points = move_points + match_points

    print(f"{decrypt(moves[0])} vs {decrypt(moves[1])} => {move_points} + {match_points} = {total_points} points")
    final_score += total_points

print(f"Final score: {final_score}")
