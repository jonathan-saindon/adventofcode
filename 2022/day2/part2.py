def get_move_points(move):
    return {
        'A': {
            'X': 3,
            'Y': 1,
            'Z': 2,
        },
        'B': {
            'X': 1,
            'Y': 2,
            'Z': 3,
        },
        'C': {
            'X': 2,
            'Y': 3,
            'Z': 1,
        }
    }[moves[0]][moves[1]]

def get_match_points(move):
    return {
        'X': 0, # Lose
        'Y': 3, # Draw
        'Z': 6  # Win
    }[move]

inputs = open('input', 'r')
lines = inputs.read().splitlines()

final_score = 0
for line in lines:
    moves = line.split(' ')

    move_points = get_move_points(moves)
    match_points = get_match_points(moves[1])
    total_points = move_points + match_points

    print(f"{move_points} + {match_points} = {total_points} points")
    final_score += total_points

print(f"Final score: {final_score}")
