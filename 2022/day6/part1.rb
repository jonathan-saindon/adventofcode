def different?(chars)
  chars.length == (chars.scan /\w/).uniq.length
end

chars = File.open('input', chomp: true, &:readline)
index = 0

while !different?(chars.slice(index..index+3)) do index += 1 end

puts index + 4
