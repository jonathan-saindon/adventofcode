def get_priority_item(items):
  (left, right) = split_bag(items)
  return find_common(left, right)
  
def split_bag(items):
  half = int(len(items) / 2)
  return (list(items[:half]), list(items[half:]))

def find_common(left, right):
  return [item for item in left if item in right][0]

def item_score(item):
  is_uppercase = item.isupper()
  score = ord(item.upper()) - 64
  return score + 26 if is_uppercase else score

inputs = open('input', 'r')
lines = inputs.read().splitlines()

total_score = 0
for items in lines:
  item = get_priority_item(items)
  score = item_score(item)
  print(f"Common item: {item} => {score}")
  total_score += score

print(f"Sum of priorities : {total_score}")