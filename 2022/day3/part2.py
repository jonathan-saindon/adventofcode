def get_groups(elfs):
  groups = []
  i = 0
  while i < len(elfs) - 2:
    groups.append([elfs[i], elfs[i + 1], elfs[i + 2]])
    i += 3
  return groups

def get_priority_item(group):
  elf0 = list(group[0])
  elf1 = list(group[1])
  elf2 = list(group[2])
  common_01 = [item for item in elf0 if item in elf1]
  common_02 = [item for item in elf0 if item in elf2]
  common_012 = [item for item in common_01 if item in common_02]
  return common_012[0]

def item_score(item):
  is_uppercase = item.isupper()
  score = ord(item.upper()) - 64
  return score + 26 if is_uppercase else score

inputs = open('input', 'r')
elfs = inputs.read().splitlines()
groups = get_groups(elfs)

total_score = 0
for group in groups:
  item = get_priority_item(group)
  score = item_score(item)
  print(f"Common item: {item} => {score}")
  total_score += score

print(f"Sum of priorities : {total_score}")
