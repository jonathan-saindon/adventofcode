const fs = require('fs');
const readline = require('readline');

async function resolve() {
  const lines = readline.createInterface({input: fs.createReadStream('input'), crlfDelay: Infinity });

  var bags = [];
  var currentBag = [];
  for await (const food of lines) {
    if (food.length === 0) {
      bags.push(currentBag);
      currentBag = [];
    }
    else {
      currentBag.push(parseInt(food));
    }
  }

  sums = bags.map(bag => bag.reduce((sum, x) => sum + x, 0));
  max = Math.max(...sums);

  console.log(max);
}

resolve();