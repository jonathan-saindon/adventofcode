bags = []
current_bag = []
lines = File.readlines('input', chomp: true).each do |food|
    if food.nil? || food.empty?
      bags << current_bag
      current_bag = []
    else
    current_bag << food.to_i
    end
end

sum = bags.map { |bag| bag.sum } # transform into sum of each bag
          .sort()                # Order from lowest to highest
          .last(3)               # Select 3 highest
          .sum                   # Sum of 3 highest

puts sum