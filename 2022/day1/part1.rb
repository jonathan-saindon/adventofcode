bags = []
current_bag = []
lines = File.readlines('input', chomp: true).each do |food|
    if food.nil? || food.empty?
      bags << current_bag
      current_bag = []
    else
    current_bag << food.to_i
    end
end

max = bags.map { |bag| bag.sum }.max
puts max
