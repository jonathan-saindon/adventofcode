const fs = require('fs');
const readline = require('readline');

async function resolve() {
  const lines = readline.createInterface({input: fs.createReadStream('input'), crlfDelay: Infinity });

  var bags = [];
  var currentBag = [];
  for await (const food of lines) {
    if (food.length === 0) {
      bags.push(currentBag);
      currentBag = [];
    }
    else {
      currentBag.push(parseInt(food));
    }
  }

  sum = bags.map(bag => bag.reduce((sum, x) => sum + x, 0)) // Transform into sum of each bag
            .sort((a, b) => a - b )                         // Sort by int value
            .slice(-3)                                      // Select 3 highest
            .reduce((sum, x) => sum + x, 0);                // Sum of 3 highest

  console.log(sum);
}

resolve();