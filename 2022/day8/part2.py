import numpy as np

def score(trees, x, y):
  h = trees[x][y]
  max_y = len(trees[x])
  max_x = len(trees)
  row = trees[x]
  column = trees[:,y]
  total_score = 1
  total_score *= sub_score(row[0:y][::-1], h) # left
  total_score *= sub_score(row[y+1:max_y], h) # right
  total_score *= sub_score(column[0:x][::-1], h) # up
  total_score *= sub_score(column[x+1:max_x], h) # down
  return total_score

def sub_score(trees, h):
  score = 0
  for tree in trees:
    if tree >= h: return (score + 1)
    else: score += 1
  return score

inputs = open('input', 'r')
lines = inputs.read().splitlines()

trees = []
for line in lines:
  trees.append(list(map(int, line)))

trees = np.array(trees)
trees_y = len(trees[0])
trees_x = len(trees)

best = 0

for x in range(1, trees_x - 1):
  for y in range(1, trees_y - 1):
    new_score = score(trees, x, y)
    if new_score > best: best = new_score

print(best)
