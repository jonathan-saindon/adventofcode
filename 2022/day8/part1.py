import numpy as np

def is_visible(trees, x, y):
  h = trees[x][y]
  max_y = len(trees[x])
  max_x = len(trees)
  row = trees[x]
  column = trees[:,y]
  # look left
  if len([a for a in row[0:y] if a >= h]) == 0: return True
  # look right
  if len([a for a in row[y+1:max_y] if a >= h]) == 0: return True
  # look up
  if len([a for a in column[0:x] if a >= h]) == 0: return True
  # look down
  if len([a for a in column[x+1:max_x] if a >= h]) == 0: return True
  return False

inputs = open('input', 'r')
lines = inputs.read().splitlines()

trees = []
for line in lines:
  trees.append(list(map(int, line)))

trees = np.array(trees)
trees_y = len(trees[0])
trees_x = len(trees)

counter = 2 * (trees_x + trees_y) - 4 # Start with perimeter count

for x in range(1, trees_x - 1):
  for y in range(1, trees_y - 1):
    if is_visible(trees, x, y): counter += 1

print(counter)
