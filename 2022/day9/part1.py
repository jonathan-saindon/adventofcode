def parse_line(line):
  parts = line.split(' ')
  return (parts[0], int(parts[1]))

def move_head(head, direction):
  x = head[0]
  y = head[1]
  if direction == 'L':   x -= 1
  elif direction == 'R': x += 1
  elif direction == 'U': y += 1
  elif direction == 'D': y -= 1
  return (x, y)

def move_tail(head, tail, direction):
  if touches(head, tail): return tail
  x = head[0]
  y = head[1]
  if direction == 'L':   x += 1
  elif direction == 'R': x -= 1
  elif direction == 'U': y -= 1
  elif direction == 'D': y += 1
  return (x, y)

def touches(head, tail):
  x_range = range(head[0] - 1, head[0] + 2)
  if len([x for x in list(x_range) if x == tail[0]]) == 0: return False
  y_range = range(head[1] - 1, head[1] + 2)
  if len([y for y in list(y_range) if y == tail[1]]) == 0: return False
  return True

inputs = open('input', 'r')
lines = inputs.read().splitlines()

# marks = [[.] * 6] * 6
head = (0,0)
tail = (0,0)
positions = []

for line in lines:
  (direction, distance) = parse_line(line)
  for n in range(0, distance):
    print(f"{head} => {tail}")
    head = move_head(head, direction)
    tail = move_tail(head, tail, direction)
    positions.append(tail)

unique_positions = list(set(positions))
print(unique_positions)
print(len(unique_positions))
