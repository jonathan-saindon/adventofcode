measurements = IO.readlines('input', chomp: true)

count = measurements.map(&:to_i)
  .each_cons(3).map { |window| window.sum }
  .each_cons(2).sum { |prev, cur| cur > prev ? 1 : 0 }

puts count
