count = 0
previous_line = ''
File.foreach('input', chomp: true).with_index do |line, index|
  if index == 0
    previous_line = line
    next
  end

  if index >= 1
    count += 1 if line.to_i > previous_line.to_i
  end
  
  
  previous_line = line
end

puts count
